console.log("index.js loaded");

var links = ["https://www.marvel.com/", "https://marvel.fandom.com/wiki/Marvel_Database", 
    "https://www.youtube.com/watch?v=6ZfuNTqbHE8", "https://www.youtube.com/watch?v=TcMBFSGVi1c",
    "https://www.youtube.com/watch?v=RxAtuMu_ph4", "https://www.youtube.com/watch?v=dxWvtMOGAhw",
    "https://www.youtube.com/watch?v=Lt-U_t2pUHI", "https://www.youtube.com/watch?v=4VSx2E7WE50",
    "https://www.youtube.com/watch?v=BsPh-nhx-Hs", "https://www.youtube.com/watch?v=SUrocDwxz_Q",
    "https://www.youtube.com/watch?v=T9GFyZ5LREQ", "https://www.youtube.com/watch?v=FMEn1N2KvZA",
    "https://www.youtube.com/watch?v=NHpWF47voGw", "https://www.youtube.com/watch?v=ZNLEh_glSQU",
    "https://www.youtube.com/watch?v=rrwBnlYOp4g", "https://www.marvelhq.com/"];

const link = document.querySelector('#link');
link.href = links[rng(0, 16)];

function rng(min, max){
    // Random number generator
    return Math.floor(Math.random() * (max - min)) + min;
}