console.log("appearances-quiz.js loaded");

var quizAnswers = [];
quizAppearances();

function quizAppearances(){
    for (var i = 0; i < 5; i++){
        question(i);
    }

    // console.log(quizAnswers);
    const btn = document.getElementById('submit-button');
    btn.addEventListener('click', result);
}

function charLookup(selectedChar, questionnum, labelnum, textnum){
    var url = "http://localhost:51044/character/" + selectedChar;
	var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    // console.log('url: ' + url);
    // console.log('xhr: ', xhr);

	xhr.onload = function(e){
		// console.log('network response recieved' + xhr.responseText);
		var jsonResponse = JSON.parse(xhr.responseText);
		var name = jsonResponse['name/alias'];
        var answer = jsonResponse['appearances'];

        // If null name retry charLookup
        if (!name) {
            charLookup(rng(0, 173), questionnum, labelnum, textnum);
        }
        
        // console.log(name, answer);
        answers(name, answer, questionnum, labelnum, textnum);
	}

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function question(i){
    var n = rng(0, 173);
    var questionnum = (i + 1) + ".";
    var labelnum = i * 4;
    var textnum = i * 3;

    charLookup(n, questionnum, labelnum, textnum);
}

function answers(character, answer, questionnum, labelnum, textnum){
    var n = rng(0, 3);
    var wrongAnswers = [rng(1, 1000), rng(1000, 3000)];
    while (true){
        if (answer == wrongAnswers[0]){
            wrongAnswers[0] = rng(1, 1000);
            continue;
        }
        if (answer == wrongAnswers[1]){
            wrongAnswers[1] = rng(1000, 3000);
            continue;
        }
        break;
    }

    // Ask question
    var label = document.getElementsByTagName("label");
    label[labelnum].innerHTML = questionnum + " How many times has " + titleCase(character) + " appeared in the comics?";

    // Place answer into n spot
    var button = document.getElementsByTagName("text");
    button[n + textnum].innerHTML = answer;

    // Place wrong answers into unused buttons of page
    var i, j = 0;
    for (i = textnum; i <= textnum + 2; i++){
        if(i != n + textnum){
            button[i].innerHTML = wrongAnswers[j];
            j++;
        }
    }

    quizAnswers.push(n);
}

function rng(min, max){
    // Random number generator
    return Math.floor(Math.random() * (max - min)) + min;
}

function titleCase(str){
    // Change strings to title case
    return str.toLowerCase().split(' ').map(function(word) {
      return (word.charAt(0).toUpperCase() + word.slice(1));
    }).join(' ');
}

function result(){
    console.log("in result");
    var numCorrect = 0;

    // Loop through all radio buttons to see if they are checked
    for (var i = 1; i < 6; i++){
        for (var j = 0; j < 3; j++){
            // console.log('bsr-radios-' + i + '-' + j);
            if (document.getElementById('bsr-radios-' + i + '-' + (j + 1)).checked){
                // console.log(i, j);
                if (j == quizAnswers[i - 1]) {
                    numCorrect++;
                }
            }
            else{
                continue;
            }
        }
    }

    // Change the results text
    // console.log('numCorrect: ', numCorrect);
    const res = document.getElementById('results');
    res.innerHTML = "You got " + numCorrect + " correct answers";
}