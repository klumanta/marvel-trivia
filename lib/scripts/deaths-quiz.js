console.log("deaths-quiz.js loaded");

var quizAnswers = [];
quizDeaths();

function quizDeaths(){
    for (var i = 0; i < 5; i++){
        question(i);
    }

    // console.log(quizAnswers);
    const btn = document.getElementById('submit-button');
    btn.addEventListener('click', result);
}

function charLookup(selectedChar, questionnum, labelnum, textnum){
    var url = "http://localhost:51044/character/" + selectedChar;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    // console.log('url: ' + url);
    // console.log('xhr: ', xhr);

    xhr.onload = function(e){
        // console.log('network response recieved' + xhr.responseText);
        var jsonResponse = JSON.parse(xhr.responseText);
        var totalDeaths;
        var name = jsonResponse['name/alias'];
        var answer5 = jsonResponse['death5'];
        totalDeaths = 5;
        if (answer5 = null){
            var answer4 = jsonResponse['death4'];
            totalDeaths = 4;
            if(answer4 = null){
                var answer3 = jsonResponse['death3'];
                totalDeaths = 3;
                if(answer3 = null){
                    var answer2 = jsonResponse['death2'];
                    totalDeaths = 2;
                    if(answer2 = null){
                        var answer1 = jsonResponse['death4'];
                        totalDeaths = 1;
                        if(answer1 = null){
                            totalDeaths = 0;
                        }
                    }
                }
            }
        }

        if (!name) {
            charLookup(rng(0, 173), questionnum, labelnum, textnum);
        }
        console.log(name, totalDeaths);
        answers(name, totalDeaths, questionnum, labelnum, textnum);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function question(i){
    var n = rng(0, 173);
    var questionnum = (i + 1) + ".";
    var labelnum = i * 4;
    var textnum = i * 3;

    char_appear = charLookup(n, questionnum, labelnum, textnum);
}

function answers(character, totalDeaths, questionnum, labelnum, textnum){
    var n = rng(0, 2);
    var wrongAnswers = [rng(1, 5), rng(5, 10)];
    while (true){
        if (totalDeaths == wrongAnswers[0]){
            wrongAnswers[0] = rng(1, 5);
            continue;
        }
        if (totalDeaths == wrongAnswers[1]){
            wrongAnswers[1] = rng(5, 10);
            continue;
        }
        break;
    }

    // Ask question
    var label = document.getElementsByTagName("label");
    label[labelnum].innerHTML = questionnum + " How many times has " + titleCase(character) + " died in the comics?";

    // Place answer into n spot
    var button = document.getElementsByTagName("text");
    button[n + textnum].innerHTML = totalDeaths;

    // Place wrong answers into unused buttons of page
    var i, j = 0;
    for (i = textnum; i <= textnum + 2; i++){
        if(i != n + textnum){
            button[i].innerHTML = wrongAnswers[j];
            j++;
        }
    }

    return n;
}

function rng(min, max){
    return Math.floor(Math.random() * (max - min)) + min;
}

function titleCase(str) {
    return str.toLowerCase().split(' ').map(function(word) {
      return (word.charAt(0).toUpperCase() + word.slice(1));
    }).join(' ');
}

function result(){
    console.log("in result");
    var numCorrect = 0;

    // Loop through all radio buttons to see if they are checked
    for (var i = 1; i < 6; i++){
        for (var j = 0; j < 3; j++){
            // console.log('bsr-radios-' + i + '-' + j);
            if (document.getElementById('bsr-radios-' + i + '-' + (j + 1)).checked){
                // console.log(i, j);
                if (j == quizAnswers[i - 1]) {
                    numCorrect++;
                }
            }
            else{
                continue;
            }
        }
    }

    // Change the results text
    // console.log('numCorrect: ', numCorrect);
    const res = document.getElementById('results');
    res.innerHTML = "You got " + numCorrect + " correct answers";
}