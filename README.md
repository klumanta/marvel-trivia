# marvel-trivia

Marvel trivia on a web page using a REST API.

To run the server use the following command while inside the server folder: python ./server.py

Scale and complexity:

The quizzes themselves are fairly complex, with questions being randomly generated every time the page is loaded, as well as the answers and their placements varying each time. To help automate things many functions to call upon, such as question, answer, rng, and titleCase. We originally also planned to have an add and delete page with accompanying network calls, but we did not have enough time to implement them.

Request Type | Resource Endpoint | Body | Expected Response | Inner Working of Handler 
 --- | --- | --- | --- | --- 
GET | /character/ | There should be no body to a GET request | {“result”: “success”, “characters”: ["appearances": 1269, "current?": "yes", "death1": "yes", "name”: “Hank Pym”, …]} | GET INDEX gets all char’s data
GET | /character/0 | There should be no body to a GET request | {“result”: “success”, "appearances": 1269, "current?": "yes", "death1": "yes", "name”: “Hank Pym”, ...} | GET KEY gets that char’s data  
PUT | /character/0 | {"appearances": 1269, "current?": "yes", "death1": "yes", "name”: “Ant Man”, ...} | {“result”: “success”} if the operation worked | PUT KEY updates that char entry 
POST | /character/ | {"appearances": 69, "current?": "yes", "death1": "no", "name”: “ABC Man”, ...} | {“result”: “success”,  “cid”: 178} if the operation worked | POST INDEX creates a new char entry in mdb and returns the new id. new id should be max+1 of all existing movie ids
DELETE | /character/101 | ‘{}’ | {“result”: “success”} if the operation worked | DELETE KEY calls delete char on the char id
DELETE | /character/ | ‘{}’ | {“result”: “success”} if the operation worked | DELETE INDEX goes through and deletes all characters from mdb







