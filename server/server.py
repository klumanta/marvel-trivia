#simple server that provides dictionary as a service
import routes
import cherrypy
from charController import characterController

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "GET, DELETE, PUT, POST"
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "null"

def start_service():

    #create option to connect with controllers
    c_cont = characterController()

    #create dispatcher, use dispatcher to connect resource endpoints to even handler
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # gets all characters' data
    dispatcher.connect('GET_INDEX', '/character/', controller=c_cont, action='GET_INDEX', conditions=dict(method=['GET']))

    # gets a specific character's data
    dispatcher.connect('GET_KEY', '/character/:key', controller=c_cont, action='GET_KEY', conditions=dict(method=['GET']))

    # updates a character
    dispatcher.connect('PUT_KEY', '/character/:name', controller=c_cont, action='PUT_KEY', conditions=dict(method=['PUT']))

    # creates a new character entry in mdb and retyrns the new id
    dispatcher.connect('POST_INDEX', '/character/', controller=c_cont, action='POST_INDEX', conditions=dict(method=['POST']))

    # deletes and individual character
    dispatcher.connect('DELETE_KEY', '/character/:name', controller=c_cont, action='DELETE_KEY', conditions=dict(method=['DELETE']))

    # deletes all characters from mdb
    dispatcher.connect('DELETE_INDEX', '/character/', controller=c_cont, action='DELETE_INDEX', conditions=dict(method=['DELETE']))


    # set up configuration
    conf = {
            'global' : {
                'server.socket_host': 'localhost',
                'server.socket_port': 51044,
                },
            '/' : {
                'request.dispatch': dispatcher,
                'tools.CORS.on' : True,
            }
    } # end of conf

    #update with new configuration
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)

    #start the server
    cherrypy.quickstart(app)

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
