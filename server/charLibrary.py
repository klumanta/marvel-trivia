import os
import json

class char_database:
    def __init__(self):
        # Each entry of the list is a dictionary of a character
        self.char_data = list()

    def load_char_data(self, file_name):
        f = open(file_name,'r')
        raw_data = json.load(f)
        # We only keep the following information for each character
        data_we_want = ['appearances', 'current?', 'death1', 'death2',
                        'death3', 'death4', 'death5', 'full/reserve avengers intro',
                        'gender', 'name/alias', 'notes', 'return1',
                        'return2', 'return3', 'return4', 'return5', 'year']
        for characters in raw_data:
            info = dict()
            for key in characters.keys():
                if key in data_we_want:
                    info[key] = characters[key]
            self.char_data.append(info)
        f.close()

    def get_chars(self):
        # Get all characters from the list
        return self.char_data

    def get_char(self, cid):
        # Get a specific character from mid
        try:
            char = self.char_data[cid]
        except Exception as ex:
            char = None
        return char
    
    def set_char(self, name, character):
        i = 0
        for char in self.char_data:
            if char['name/alias'] == name:
                self.char_data[i] = character
            i += 1
        
    def add_char(self, char):
        self.char_data.append(char)
    
    def delete_char(self, cid):
        self.char_data[cid] = {}
        
    def clear_list(self):
        self.char_data = list()

if __name__ == "__main__":
    char = char_database()
    file_name = 'characters.json'
    char.load_char_data(file_name)
    print(char.char_data)