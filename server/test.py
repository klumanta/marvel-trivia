import requests
import json


if __name__ == "__main__":
    SITE_URL = 'http://localhost:51044'
    print("Testing for server: " + SITE_URL)
    CHARS_URL = SITE_URL + '/character/'

    # Test get with cid
    cid = 0
    r = requests.get(CHARS_URL + str(cid))
    resp = json.loads(r.content.decode())
    # print(resp)

    # Test get index
    r = requests.get(CHARS_URL)
    resp = json.loads(r.content.decode())
    # print(resp)

    # Test get with name
    name = "henry jonathan \"hank\" pym"
    r = requests.get(CHARS_URL + name)
    resp = json.loads(r.content.decode())
    # print(resp)

    # Test put
    resp['current?'] = 'no'
    resp['gender'] = 'female'
    r = requests.put(CHARS_URL + name, data = json.dumps(resp))
    # print(r.text)
    r = requests.get(CHARS_URL + name)
    resp = json.loads(r.content.decode())
    # print(resp)

    # Test post
    new_name = 'ant man'
    resp['name/alias'] = new_name
    r = requests.post(CHARS_URL, data = json.dumps(resp))
    resp = json.loads(r.content.decode())
    r = requests.get(CHARS_URL + new_name)
    resp = json.loads(r.content.decode())
    # print(resp)
    
    # Test delete name
    r = requests.delete(CHARS_URL + new_name)
    print(r.text)
    resp = json.loads(r.content.decode())
    print(resp)
    r = requests.get(CHARS_URL + new_name)
    print(r.text)
    resp = json.loads(r.content.decode())
    print(resp)

    # Test delete index
    r = requests.delete(CHARS_URL)
    resp = json.loads(r.content.decode())
    print(resp)
    r = requests.get(CHARS_URL)
    print(r.text)
