import cherrypy
import re, json
from charLibrary import char_database

class characterController(object):

    def __init__(self, chardb=None):
        if chardb is None:
            self.chardb = char_database()
        else:
            self.chardb = chardb

        self.chardb.load_char_data('characters.json')

    def GET_INDEX(self):
        output = {'result' : 'success'}

        output['characters'] = []

        try:
            for char in self.chardb.get_chars():
                output['characters'].append(char)

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def GET_KEY(self, key):
        output = {'result' : 'success'}

        try:
            key = int(key)
            i = 0
            for char in self.chardb.get_chars():
                if i == key:
                    output.update(char)
                    return json.dumps(output)
                i += 1

            output['result'] = 'error'
            output['message'] = 'character not found'

        except ValueError as ex:
            for char in self.chardb.get_chars():
                try:
                    if char['name/alias'].lower() == key:
                        output.update(char)
                        return json.dumps(output)
                except:
                    continue

            output['result'] = 'error'
            output['message'] = 'No character named ' + key + ' was found.'

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, name):
        output = {'result' : 'success'}

        char = json.loads(cherrypy.request.body.read().decode('utf-8'))

        self.chardb.set_char(name, char)

        return json.dumps(output)

    def POST_INDEX(self):
        output = {'result' : 'success'}
        char = json.loads(cherrypy.request.body.read().decode('utf-8'))

        try:
            self.chardb.add_char(char)

        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def DELETE_INDEX(self):
        output = {'result' : 'success'}

        try:
            self.chardb.clear_list()
        except Exception as ex:
            output['result'] ='error'
            output['message'] = str(ex)

        return json.dumps(output)

    def DELETE_KEY(self, name):
        output = {'result' : 'success'}
        i = 0
        for char in self.chardb.get_chars():
            print('current char is', char['name/alias'])
            try:
                if char['name/alias'].lower() == name:
                    self.chardb.delete_char(i)
                    return json.dumps(output)
                i += 1
            except:
                continue

        output['result'] ='error'
        output['message'] = str(ex)

        return json.dumps(output)
